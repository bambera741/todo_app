import os, requests, json, pymongo

class Item:
    def __init__(self, id, name, status):
        self.id = id
        self.name = name
        self.status = status

    @classmethod
    def from_trello_card(cls, card):
        return cls(card['_id'], card['name'], card['status'])

def get_items():
    """
    Fetches all saved items from the MongoDB.

    Returns:
        list: The list of saved items.
    """
    client = pymongo.MongoClient(os.getenv("MONGO_CONNECTION_STRING"))
    database = client[os.getenv("MONGO_DATABASE_NAME")]
    collection = database[os.getenv("MONGO_COLLECTION")]

    cards =[]

    for card in collection.find():
        cards.append(Item.from_trello_card(card))
    return (cards)

def get_item(id):
    """
    Fetches the saved item with the specified ID.

    Args:
        id: The ID of the item.

    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    items = get_items()
    return next((item for item in items if item['id'] == int(id)), None)


def add_item(card_name):
    """
    Adds a new item with the specified title to the session.
    """

    client = pymongo.MongoClient(os.getenv("MONGO_CONNECTION_STRING"))
    database = client[os.getenv("MONGO_DATABASE_NAME")]
    collection = database[os.getenv("MONGO_COLLECTION")]
    collection.insert_one({
        'name': card_name,
        'status': 'todo'
    })

    

def save_item(item):
    """
    Updates an existing item in the session. If no existing item matches the ID of the specified item, nothing is saved.

    Args:
        item: The item to save.
    """
    existing_items = get_items()
    updated_items = [item if item['id'] == existing_item['id'] else existing_item for existing_item in existing_items]

    session['items'] = updated_items

    return item

def complete_item(id):

    client = pymongo.MongoClient(os.getenv("MONGO_CONNECTION_STRING"))
    database = client[os.getenv("MONGO_DATABASE_NAME")]
    collection = database[os.getenv("MONGO_COLLECTION")]
    
    query = {'_id':id}
    newvalues = { "$set": { "status": "done" } }
    collection.update_one(query, newvalues)

def move_to_todo(id):

    client = pymongo.MongoClient(os.getenv("MONGO_CONNECTION_STRING"))
    database = client[os.getenv("MONGO_DATABASE_NAME")]
    collection = database[os.getenv("MONGO_COLLECTION")]
    
    query = {'_id':id}
    newvalues = { "$set": { "status": "todo" } }
    collection.update_one(query, newvalues)